package main

import (
	"context"
	"encoding/csv"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"gitlab.com/doug-manley/delaware-candidate-query/internal/googleauth"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

func main() {
	var credentialsFilename string
	var outputCSV bool
	var serviceAccountCredentialsFilename string
	var sortColumnsString string
	var spreadsheetID string
	var tokenFilename string
	var year string
	flag.StringVar(&credentialsFilename, "credentials-file", "", "The Google credentials file.")
	flag.BoolVar(&outputCSV, "output-csv", false, "If set, a CSV will be written to stdout.")
	flag.StringVar(&serviceAccountCredentialsFilename, "service-account-credentials-file", "", "The Google service account credentials file.")
	flag.StringVar(&sortColumnsString, "sort-columns", "", "The columns to sort by (comma-separated); you may prefix with '+' for ascending (default) or '-' for descending.")
	flag.StringVar(&spreadsheetID, "spreadsheet-id", "", "The Google Sheets ID.")
	flag.StringVar(&tokenFilename, "token-file", "", "The Google authentication token file.")
	flag.StringVar(&year, "year", "2022", "The year to query.")
	flag.Parse()

	var sortColumns []string
	for _, column := range strings.Split(sortColumnsString, ",") {
		column = strings.TrimSpace(column)
		if column != "" {
			sortColumns = append(sortColumns, column)
		}
	}

	primaryURL := "https://elections.delaware.gov/services/candidate/prim_fcddt_" + year + ".shtml"
	generalURL := "https://elections.delaware.gov/services/candidate/genl_fcddt_" + year + ".shtml"

	var candidates [][]string
	{
		table, err := parseCandidates(primaryURL)
		if err != nil {
			logrus.Warnf("Could not get the primary candidates: [%T] %v", err, err)
		}
		table = injectColumns(table, []string{"year", "race"}, []string{year, "primary"})
		candidates = mergeTables(candidates, table)
	}
	{
		table, err := parseCandidates(generalURL)
		if err != nil {
			logrus.Warnf("Could not get the general candidates: [%T] %v", err, err)
		}
		table = injectColumns(table, []string{"year", "race"}, []string{year, "general"})
		candidates = mergeTables(candidates, table)
	}
	logrus.Infof("Candidates: %v", candidates)

	if len(candidates) >= 2 {
		headers := candidates[0]
		rows := candidates[1:]
		logrus.Infof("Headers: %v", headers)

		newFields := []string{
			"name",
			"address",
			"email",
			"website",
		}
		generalColumns := []string{
			"name, address, phone",
			"name and address",
		}

		var newHeaders []string
		for _, header := range headers {
			present := false
			for _, generalColumn := range generalColumns {
				if header == generalColumn {
					present = true
					break
				}
			}
			if !present {
				newHeaders = append(newHeaders, header)
			}
		}
		for _, newField := range newFields {
			present := false
			for _, header := range headers {
				if header == newField {
					present = true
					break
				}
			}
			if !present {
				newHeaders = append(newHeaders, newField)
			}
		}
		logrus.Infof("New headers: %v", newHeaders)

		var newRows [][]string
		for _, row := range rows {
			data := map[string]string{}
			for c, header := range headers {
				data[header] = row[c]
			}
			if value := data["date filed"]; value != "" {
				v, err := time.Parse("1/2/2006", value)
				if err != nil {
					logrus.Warnf("Could not parse date: %s", value)
				} else {
					data["date filed"] = v.Format("2006-01-02")
				}
			}
			withdrawn := false
			for _, generalColumn := range generalColumns {
				value := data[generalColumn]
				if value == "" {
					continue
				}

				lines := strings.Split(value, "\n")
				for len(lines) > 0 {
					line := lines[0]
					lines = lines[1:]

					if len(line) == 0 {
						continue
					}

					if strings.Contains(strings.ToLower(line), "withdrawn") {
						withdrawn = true
					}

					if !strings.Contains(line, ":") {
						if data["name"] == "" {
							data["name"] = line
						}
					} else {
						parts := strings.SplitN(line, ":", 2)
						key := strings.TrimSpace(strings.ToLower(parts[0]))
						var value string
						if len(parts) > 1 {
							value = strings.TrimSpace(parts[1])
						}

						if key == "address" {
							for len(lines) > 0 {
								// Peek at the next line.
								line := lines[0]

								if strings.Contains(line, ":") {
									break
								}
								if len(line) == 0 {
									// Quietly ignore the empty line.
								} else if strings.Contains(strings.ToLower(line), "withdrawn") {
									withdrawn = true
								} else {
									value += "\n" + line
								}
								lines = lines[1:]
							}
						}
						data[key] = value
					}
				}
			}

			if withdrawn {
				continue
			}

			var newRow []string
			for _, header := range newHeaders {
				newRow = append(newRow, data[header])
			}
			newRows = append(newRows, newRow)
		}

		if len(sortColumns) > 0 {
			newHeaderMap := map[string]int{}
			for c, header := range newHeaders {
				newHeaderMap[header] = c
			}

			var sortColumnIndexes []int
			var sortColumnMultipliers []int
			for _, column := range sortColumns {
				multiplier := 1
				if column != "" {
					if strings.HasPrefix(column, "+") {
						column = strings.TrimPrefix(column, "+")
						multiplier = 1
					} else if strings.HasPrefix(column, "-") {
						column = strings.TrimPrefix(column, "-")
						multiplier = -1
					}
				}
				if c, ok := newHeaderMap[column]; ok {
					sortColumnIndexes = append(sortColumnIndexes, c)
					sortColumnMultipliers = append(sortColumnMultipliers, multiplier)
				}
			}

			sort.Slice(newRows, func(i, j int) bool {
				for s, c := range sortColumnIndexes {
					multiplier := sortColumnMultipliers[s]
					left := newRows[i][c]
					right := newRows[j][c]
					difference := strings.Compare(left, right)
					if difference == 0 {
						continue
					}
					return (difference * multiplier) < 0
				}
				return true
			})
		}

		candidates = append([][]string{newHeaders}, newRows...)
	}

	if outputCSV {
		csvWriter := csv.NewWriter(os.Stdout)
		csvWriter.WriteAll(candidates)
	}

	if spreadsheetID != "" {
		logrus.Infof("Doing the Google Sheets thing.")
		ctx := context.Background()

		var serviceOptions []option.ClientOption
		if serviceAccountCredentialsFilename == "" {
			b, err := ioutil.ReadFile(credentialsFilename)
			if err != nil {
				logrus.Errorf("Unable to read client secret file: [%T] %v", err, err)
				os.Exit(1)
			}
			logrus.Infof("Read credentials.json successfully.")

			config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
			if err != nil {
				logrus.Errorf("Unable to parse client secret file to config: [%T] %v", err, err)
				os.Exit(1)
			}
			logrus.Infof("Parsed config successfully.")

			client, err := googleauth.Client(config, tokenFilename)
			if err != nil {
				logrus.Errorf("Unable to parse client secret file to config: [%T] %v", err, err)
				os.Exit(1)
			}
			logrus.Infof("Created client successfully.")

			serviceOptions = append(serviceOptions, option.WithHTTPClient(client))
		} else {
			serviceOptions = append(serviceOptions, option.WithCredentialsFile(serviceAccountCredentialsFilename))
		}
		srv, err := sheets.NewService(ctx, serviceOptions...)
		if err != nil {
			logrus.Errorf("Unable to retrieve Sheets client: [%T] %v", err, err)
			os.Exit(1)
		}

		readRange := year + "!A:Z"
		resp, err := srv.Spreadsheets.Values.Get(spreadsheetID, readRange).Do()
		if err != nil {
			logrus.Errorf("Unable to retrieve data from sheet: [%T] %v", err, err)
			os.Exit(1)
		}
		logrus.Infof("Existing values: %v", resp.Values)

		values := [][]interface{}{}
		for _, row := range candidates {
			newRow := []interface{}{}

			for _, column := range row {
				newRow = append(newRow, column)
			}

			values = append(values, newRow)
		}

		/*
			_, err = srv.Spreadsheets.Values.Clear(spreadsheetID, readRange, &sheets.ClearValuesRequest{}).Do()
			if err != nil {
				logrus.Errorf("Could not clear values: %v", err)
				os.Exit(1)
			}
		*/
		_, err = srv.Spreadsheets.Values.Update(spreadsheetID, readRange, &sheets.ValueRange{Values: values}).ValueInputOption("RAW").Do()
		if err != nil {
			logrus.Errorf("Could not set values: %v", err)
			os.Exit(1)
		}
	}
}

func parseCandidates(address string) ([][]string, error) {
	httpResponse, err := http.Get(address)
	if err != nil {
		return nil, err
	}
	defer httpResponse.Body.Close()

	doc, err := goquery.NewDocumentFromReader(httpResponse.Body)
	if err != nil {
		return nil, err
	}

	doc.Find("header").Each(func(i int, s *goquery.Selection) {
		if role, _ := s.Attr("role"); role == "banner" {
			return
		}

		var headers []string
		for _, h := range []string{"h1", "h2", "h3", "h4", "h5"} {
			header := strings.TrimSpace(s.Find(h).Text())
			if header != "" {
				headers = append(headers, header)
			}
		}
		logrus.Infof("Section headers: %v", headers)
	})

	var table [][]string
	if len(table) == 0 {
		doc.Find("table").Each(func(i int, s *goquery.Selection) {
			var headers []string
			s.Find("th").Each(func(i int, s *goquery.Selection) {
				header := strings.TrimSpace(strings.ToLower(s.Text()))
				if header != "" {
					headers = append(headers, header)
				}
			})
			logrus.Infof("Table headers: %v", headers)

			mustHaves := []string{"office", "county"}
			for _, mustHave := range mustHaves {
				okay := false
				for _, header := range headers {
					if header == mustHave {
						okay = true
						break
					}
				}
				if !okay {
					return
				}
			}
			logrus.Infof("This is a valid candidate table.")

			var rows [][]string
			s.Find("tbody tr").Each(func(i int, s *goquery.Selection) {
				var cells []string
				s.Find("td").Each(func(i int, s *goquery.Selection) {
					s.Find("br").ReplaceWithHtml("\n")
					//logrus.Infof("Cell: %s", s.Text())
					var parts []string
					for _, line := range strings.Split(strings.TrimSpace(s.Text()), "\n") {
						parts = append(parts, strings.TrimSpace(line))
					}
					cell := strings.Join(parts, "\n")
					cells = append(cells, cell)
				})
				logrus.Infof("Cells: %v", cells)
				if len(cells) != len(headers) {
					logrus.Warnf("Invalid number of cells for row %d: %d (expected %d)", i, len(cells), len(headers))
					return
				}
				rows = append(rows, cells)
			})
			logrus.Infof("Rows: (%d)", len(rows))

			table = append([][]string{headers}, rows...)
		})
	}
	if len(table) == 0 {
		doc.Find(".divTable").Each(func(i int, s *goquery.Selection) {
			var headers []string
			s.Find(".divTableHead").Each(func(i int, s *goquery.Selection) {
				header := strings.TrimSpace(strings.ToLower(s.Text()))
				if header != "" {
					headers = append(headers, header)
				}
			})
			logrus.Infof("Table headers: %v", headers)

			mustHaves := []string{"office", "county"}
			for _, mustHave := range mustHaves {
				okay := false
				for _, header := range headers {
					if header == mustHave {
						okay = true
						break
					}
				}
				if !okay {
					return
				}
			}
			logrus.Infof("This is a valid candidate table.")

			var rows [][]string
			s.Find(".divTableBody .divTableRow").Each(func(i int, s *goquery.Selection) {
				var cells []string
				s.Find(".divTableCell").Each(func(i int, s *goquery.Selection) {
					s.Find("br").ReplaceWithHtml("\n")
					//logrus.Infof("Cell: %s", s.Text())
					var parts []string
					for _, line := range strings.Split(strings.TrimSpace(s.Text()), "\n") {
						parts = append(parts, strings.TrimSpace(line))
					}
					cell := strings.Join(parts, "\n")
					cells = append(cells, cell)
				})
				logrus.Infof("Cells: %v", cells)
				if len(cells) != len(headers) {
					logrus.Warnf("Invalid number of cells for row %d: %d (expected %d)", i, len(cells), len(headers))
					return
				}
				rows = append(rows, cells)
			})
			logrus.Infof("Rows: (%d)", len(rows))

			table = append([][]string{headers}, rows...)
		})
	}

	if len(table) == 0 {
		return table, nil
	}

	return table, nil
}

func injectColumns(table [][]string, newColumns, newValues []string) [][]string {
	if len(table) == 0 {
		return table
	}

	headers := table[0]
	rows := table[1:]

	var newColumnsToAdd []string
	var newValuesToAdd []string
	{
		for i, newColumn := range newColumns {
			present := false
			for _, header := range headers {
				if header == newColumn {
					present = true
					break
				}
			}
			if !present {
				newColumnsToAdd = append(newColumnsToAdd, newColumn)
				newValuesToAdd = append(newValuesToAdd, newValues[i])
			}
		}
	}
	if len(newColumnsToAdd) == 0 {
		return table
	}

	newHeaders := append(newColumnsToAdd, headers...)
	var newRows [][]string
	for _, row := range rows {
		newRow := append(newValuesToAdd, row...)
		newRows = append(newRows, newRow)
	}
	var newTable [][]string
	newTable = append(newTable, newHeaders)
	newTable = append(newTable, newRows...)
	return newTable
}

func mergeTables(table1, table2 [][]string) [][]string {
	if len(table1) < 2 {
		return table2
	}
	if len(table2) < 2 {
		return table1
	}

	headers := [][]string{
		table1[0],
		table2[0],
	}
	rows := [][][]string{
		table1[1:],
		table2[1:],
	}

	headerMap1 := map[string]bool{}
	for _, header := range headers[0] {
		headerMap1[header] = true
	}
	newHeaders := headers[0]
	for _, header := range headers[1] {
		if !headerMap1[header] {
			newHeaders = append(newHeaders, header)
		}
	}

	var newRows [][]string
	for x, rowsX := range rows {
		for _, row := range rowsX {
			data := map[string]string{}
			for i, header := range headers[x] {
				data[header] = row[i]
			}
			var newRow []string
			for _, header := range newHeaders {
				newRow = append(newRow, data[header])
			}
			newRows = append(newRows, newRow)
		}
	}

	var newTable [][]string
	newTable = append(newTable, newHeaders)
	newTable = append(newTable, newRows...)
	return newTable
}
