module gitlab.com/doug-manley/delaware-candidate-query

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/oauth2 v0.0.0-20220411215720-9780585627b5
	google.golang.org/api v0.30.0
)
